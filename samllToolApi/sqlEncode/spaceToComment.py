#!/usr/bin/env python
# -*- coding:utf-8 -*-

def description():
    contentVal = '''
    @des
    将空格替换为注释[/**/]
    可正常解析的数据库版本
        * Microsoft SQL Server 2005
        * MySQL 4, 5.0 and 5.5
        * Oracle 10g
        * PostgreSQL 8.3, 8.4, 9.0
    @notes
    用于测试薄弱的防护规则    
    '''
    return contentVal
def sqlEncode(payload,**kwargs):
    """
    @des
    将空格替换为注释[/**/]
    可正常解析的数据库版本
        * Microsoft SQL Server 2005
        * MySQL 4, 5.0 and 5.5
        * Oracle 10g
        * PostgreSQL 8.3, 8.4, 9.0
    @notes
    用于测试薄弱的防护规则    
    """
    retVal = payload

    if payload:
        retVal = ""
        quote, doublequote, firstspace = False, False, False
        for i in range(len(payload)):
            if not firstspace:
                if payload[i].isspace():
                    firstspace = True
                    retVal += "/**/"
                    continue
            elif payload[i] == '\'':
                quote = not quote            
            elif payload[i] == '"':
                doublequote = not doublequote
            elif payload[i] == " " and not doublequote and not quote:
                retVal += "/**/"
                continue
            retVal += payload[i]
    return retVal
'''
codeStr = " 1 2 3 "
print(sqlEncode(codeStr))
'''