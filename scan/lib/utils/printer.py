#!/usr/bin/env python
# -*- coding:utf-8 -*-

import lib.utils.colorForUnix as cu
import lib.utils.colorForWin as cw
from lib.utils.check import *


def plus(s,flag="[+]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printGreen(v)
    else:
        print("{}{}{} {}{}{}".format(
        cu.GREEN%(0),flag,cu.RESET,
		cu.WHITE%(0),s,cu.RESET))

def less(s,flag="[-]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printRed(v)
    else:
        print("{}{}{} {}{}{}".format(
            cu.RED%(0),flag,cu.RESET,
            cu.WHITE%(0),s,cu.RESET
            ))

def warn(s,flag="[!]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printRed(v)
    else:
        print("{}{}{} {}{}{}".format(
		cu.RED%(0),flag,cu.RESET,
		cu.RED%(0),s,cu.RESET
		))

def test(s,flag="[*]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printBlue(v)
    else:
        print("{}{}{} {}{}{}".format(
		cu.BLUE%(0),flag,cu.RESET,
		cu.WHITE%(0),s,cu.RESET
		))

def info(s,flag="[i]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printYellow(v)
    else:
        print("{}{}{} {}{}{}".format(
		cu.YELLOW%(0),flag,cu.RESET,
		cu.WHITE%(0),s,cu.RESET
		))
def info_nothing(flag="[N]"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + "未检测到漏洞,可尝试修改逻辑或更新有效载荷!" +"\n"
        cw.printYellow(v)
    else:
        print("{}{}{} {}{}{}".format(
		cu.YELLOW%(0),flag,cu.RESET,
		cu.WHITE%(0),"未检测到漏洞,可尝试修改逻辑或更新有效载荷!",cu.RESET
		))    
def more(s,flag="|"):
    op = Cplatform()
    if op == "win":
        v = flag + " " + s +"\n"
        cw.printWhite(v)
    else:
        print("{}{}{} {}{}{}".format(
		cu.WHITE%(0),flag,cu.RESET,
		cu.WHITE%(0),s,cu.RESET
		))               

def null():
    print("")
