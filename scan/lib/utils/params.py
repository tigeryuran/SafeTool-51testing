#!/usr/bin/env python
# -*- coding:utf-8 -*-

import urllib
from re import sub,I

class preplace:

    def __init__(self,url,payload,data:str):
        self.url = url
        self.data = data
        self._params = []
        self.payload = payload
    
    def get(self):
        url_params = self.url.split("?")[1].split("&")
        for param in url_params:
            params = param.split("=")
            if len(params) > 1:
                ppayload = param.replace(param.split("=")[1],
                           urllib.parse.quote_plus(self.payload))
                porignal = param.replace(ppayload.split("=")[1],
                param.split("=")[1])
                self._params.append(sub(porignal,ppayload,self.url))
    
    def post(self):
        params = self.data.split("&")
        for param in params:
            ppayload = param.replace(param.split("=")[1],
                    urllib.parse.quote_plus(self.payload))
            porignal = param.replace(ppayload.split("=")[1],
                    param.split("=")[1])
            self._params.append(self.data.replace(porignal,ppayload))

    def run(self):
        if "?" in self.url and self.data == None:
            self.get()
        elif "?" not in self.url and self.data != None:
            self.post()
        else:
            self.get()
            self.post()
        return self._params

class padd:

    def __init__(self,url,payload,data:str):
        self.url = url
        self.payload = payload
        self.data = data
        self._params = []

    def get(self):
        url_params = self.url.split("?")[1].split("&")
        for param in url_params:
            params = param.split("=")
            if len(params) > 1:
                ppayload = param.replace(
                    params[1],
                    params[1] + urllib.parse.quote_plus(self.payload)
                )
                porignal = param.replace(ppayload.split("=")[1],params[1])
                self._params.append(sub(porignal,ppayload,self.url))
    
    def post(self):
        url_params = self.data.split("&")
        for param in url_params:
            params = param.split("=")
            ppayload = param.replace(
                        params[1],
                        params[1] + urllib.parse.quote_plus(self.payload)
                    )
            
            porignal = param.replace(ppayload.split("=")[1],params[1])
            self._params.append(self.data.replace(porignal,ppayload))

    def run(self):
        if "?" in self.url and self.data == None:
            self.get()
        elif "?" not in self.url and self.data != None:
            self.post()
        else:
            self.get()
            self.post()
        return self._params           