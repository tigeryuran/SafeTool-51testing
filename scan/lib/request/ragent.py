#!/usr/bin/env python 
# -*- coding:utf-8 -*-

from os import path
from random import randint
from lib.utils.readfile import *

def ragent():
    user_agents = ()
    realpath = path.join(path.realpath(__file__).split('lib')[0],'lib/db/')
    realpath += "useragent.txt"
    for _ in readfile(realpath):
        user_agents += (_,)
    return user_agents[randint(0,len(user_agents)-1)]