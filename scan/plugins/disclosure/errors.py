#!/usr/bin/env python 
# -*- coding:utf-8 -*-
import os
import sys
from lib.request.request import *
from lib.utils.printer import *
from lib.parser.parse import *
from lib.utils.settings import REG_ERRORS
from re import search,I

class errors(Request):
    """
    检测错误响应中的敏感信息 
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'errors':None
        }
    def check(self):
        info("检测错误响应中的敏感信息[errors]...")
        req = self.Send(url=self.url,method=self.get)
        self.pErrors(req.content)
        return self.result
    def pErrors(self,content):
        isNothing = True
        for p in REG_ERRORS:
            more("检测载荷:{},{}".format(self.url,p))
            if search(p,content):
                plus("找到敏感信息: \"%s\" 在 %s"%(p,url))
                self.result['errors'] = p
                isNothing = False
        if isNothing:
            info_nothing()
def run(kwargs,url,data):
    result = {}
    scan = errors(kwargs,url,data)
    result = scan.check()
    return result