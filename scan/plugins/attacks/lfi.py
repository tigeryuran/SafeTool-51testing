#!/usr/bin/env python
# -*- coding:utf-8 -*-

from os import path,sep
from json import loads
from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.utils.payload import *
from lib.request.request import *
from lib.utils.settings import ERRORS_PATH

class lfi(Request):
    """
    本地文件包含漏洞
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'lfi':None
        }
    def search(self,resp,content):
        for error in content['info']['regexp']:
            if search(error,resp):
                _ = content['info']['name']
                return _
    def serror(self,resp):
        _ = None
        realpath = ERRORS_PATH
        abspath = realpath + sep +"lfi.json"
        _ = self.search(resp,loads(readfile(abspath)[0],encoding="utf-8"))
        if _ != None: return _
    
    def check(self):
        info("检测本地文件包含漏洞...")
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True
        for payload in plfi():
            if self.data:
                rPayload = preplace(self.url,payload,self.data)
                for data in rPayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data=data)
                    error = self.serror(req.content)
                    if error:
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            else:
                urls = preplace(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    error = self.serror(req.content)
                    if error:
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似存在本地文件包含漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['lfi'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("疑似存在本地文件包含漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['lfi'] = PAYLOAD
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result

def run(kwargs,url,data):
    result = {}
    scan = lfi(kwargs,url,data)
    result = scan.check()
    return result
