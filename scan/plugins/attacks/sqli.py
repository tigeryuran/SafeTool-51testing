#!/usr/bin/env python
# -*- coding:utf-8 -*-

from json import loads
from re import search,I
from os import path,listdir,sep
from lib.utils.params import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.request.request import *
from lib.utils.payload import *
from lib.utils.settings import SQLDBERRORS_PATH


class sqli(Request):
    """
    SQL注入漏洞
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'sqli':None
        }

    def search(self,resp,content):
        for error in content['db']['regexp']:
            if search(error,resp):
                _ = content['db']['name']
                return _

    def serror(self,resp):
        _ = None
        realpath = SQLDBERRORS_PATH
        for f in listdir(realpath):
            abspath = realpath + sep + f
            _ = self.search(resp,loads(readfile(abspath)[0],encoding="utf-8"))
            if _ != None: return _
    
    def check(self):
        info("检测SQL注入漏洞...")
        DB = None
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True
        for payload in sql():
            if self.data:
                rPayload = padd(self.url,payload,self.data)
                for data in rPayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data=data)
                    error = self.serror(req.content)
                    if error:
                        DB = error
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            else:
                urls = padd(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    error = self.serror(req.content)
                    if error:
                        DB=error
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似存在SQL注入漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    more("DBMS[数据库]: {}".format(DB))
                    self.result['sqli'] = "{},{}".format(DB,PAYLOAD)
                    isNothing = False
                elif DATA == None:
                    plus("疑似存在SQL注入漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    more("DBMS[数据库]: {}".format(DB))
                    self.result['sqli'] = "{},{}".format(DB,PAYLOAD)
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = sqli(kwargs,url,data)
    result = scan.check()
    return result