#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import *

class xxe(Request):
    """
    XXE XML外部实体注入漏洞
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'xxe':None
        }
    
    def check(self):
        info("检测XXE[XML外部实体]注入漏洞...")
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True

        for payload in xxep():
            if self.data:
                addPayload = preplace(self.url,payload,self.data)
                for data in addPayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data=data)
                    if search(payload,req.content):
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            else:
                urls = preplace(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    if search(payload,req.content):
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似存在XXE漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['xxe'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("疑似存在XXE漏洞:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['xxe'] = PAYLOAD
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result

def run(kwargs,url,data):
    result = {}
    scan = xxe(kwargs,url,data)
    result = scan.check()
    return result