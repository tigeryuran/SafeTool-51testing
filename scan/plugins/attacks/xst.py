#!/usr/bin/env python
# -*- coding:utf-8 -*-

from re import search,I
from lib.utils.printer import *
from lib.request.request import *

class xst(Request):
    """
    xst/cst 跨站跟踪漏洞
    """
    trace = "TRACE"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result ={
            'xst':None
        }

    def check(self):
        info("检测跨站跟踪漏洞...")
        headers = {
        'XST':'is_Xst'
        }
        isNothing = True
        more("检测载荷:{},header:{}".format(self.url,str(headers)))
        req = self.Send(url=self.url,method=self.trace,headers=headers)
        regexp = r"*?is_Xst$"
        if 'XST' in req.headers or 'xst' in req.headers:
            if search(regexp,req.headers['XST'],I):
                plus('疑似存在跨站跟踪漏洞[xst/cst]: %s'%(req.url))
                self.result['xst'] = req.url
                isNothing = False
        if isNothing:
            info_nothing()
        return self.result

def run(kwargs,url,data):
    result = {}
    scan = xst(kwargs,url,data)
    result = scan.check()
    return result
