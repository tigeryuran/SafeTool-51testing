#!/usr/bin/env python
# -*- coding:utf-8 -*-

from os import path
from re import search
from lib.utils.check import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.request.request import *
from lib.utils.settings import PHPINFO_PATH
class phpinfo(Request):
    """
    检测 phpinfo
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'phpinfo':None
        }

    def check(self):
        info("检测 phpinfo信息...")
        isNothing = True
        splitUrl = SplitUrl(self.url)
        netlocUrl = splitUrl.netloc
        for path in readfile(PHPINFO_PATH):
            url = Cpath(netlocUrl,path.decode('utf-8'))
            more("检测载荷:{}".format(url))
            req = self.Send(url=url,method=self.get)
            if req.code == 200:
                if search(r'\<title\>phpinfo()\<\/title\>|\<h1 class\=\"p\"\>PHP Version',req.content):
                    plus('找到phpinfo页面: %s'%(req.url))
                    self.result['phpinfo'] = req.url
                    isNothing = False
                    break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = phpinfo(kwargs,url,data)
    result = scan.check()
    return result

