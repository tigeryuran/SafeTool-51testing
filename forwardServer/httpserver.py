#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer
from os import path
from urllib.parse import urlparse
import urllib
import json
import re
import random
from socketserver import ThreadingMixIn
from mysite import urls,settings
from mysite.utils.formatTools import get_app_name
import importlib
sep = '/'
appPath = settings.APP_PATH
installApp = settings.INSTALLED_APP
codeErr = settings.CODE_ERROR
codeScene = settings.CODE_SCENE
# MIME-TYPE
mimedic = [
                        ('.html', 'text/html'),
                        ('.htm', 'text/html'),
                        ('.js', 'application/javascript'),
                        ('.css', 'text/css'),
                        ('.json', 'application/json'),
                        ('.png', 'image/png'),
                        ('.jpg', 'image/jpeg'),
                        ('.gif', 'image/gif'),
                        ('.txt', 'text/plain'),
                        ('.avi', 'video/x-msvideo'),
                        ('.ttf', 'image/ttf'),
                        ('.woff', 'image/woff'),
                        ('.woff2', 'image/woff2'),
                        ('.ico', 'image/ico'),

                    ]

COUNTS = 0
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
    response = {
        'code': 0,
        'msg':'',
        'data':{}
    }
    # GET
    def do_GET(self):
        querypath = urlparse(self.path)
        filepath, query = querypath.path, querypath.query
        self.match_router_get(filepath,query)
    # POST
    def do_POST(self):
        datas = self.rfile.read(int(self.headers['content-length']))
        isJson = self.is_josn(str(datas,encoding='utf-8'))
        if isJson:
            jsonParam = json.loads(datas)
            self.match_router_post(self.path,jsonParam)
        else:
            self.send_json_fail(codeErr['5'],"参数错误!")
    def set_json_header(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin','*')#允许跨域
        self.end_headers()
    def get_byte_resp(self,resp:str):
        return resp.encode()
    def match_router_get(self,path,params):
        err = True
        for p in urls.get_url.keys():
            if p == path:
                appName = get_app_name(path.split("/"))
                for a in installApp:
                    if appName == a:
                        apis,func = urls.get_url[path].split("&")
                        app = appPath+"."+appName+"."+apis
                        appApi = importlib.import_module(app)
                        appFunc = getattr(appApi,func)
                        appFunc(self,params)
                        err = False
        if err:
            self.send_json_fail(codeErr['1'],"请求不存在!")
    def match_router_post(self,path,params):
        err = True
        for p in urls.post_url.keys():
            if p == path:
                appName = get_app_name(path.split("/"))
                for a in installApp:
                    if appName == a:
                        apis,func = urls.post_url[path].split("&")
                        app = appPath+"."+appName+"."+apis
                        appApi = importlib.import_module(app)
                        appFunc = getattr(appApi,func)
                        appFunc(self,params)
                        err = False
        if err:
            self.send_json_fail(codeErr['1'],"请求不存在!")        
    def is_josn(self,data):
        try:
            json.loads(data)
        except ValueError:
            return False
        return True        
    def send_json_success(self,data:dict):
        self.response['code'] = 0
        self.response['msg'] = ''
        self.response['data'] = data
        self.set_json_header()
        resp = self.get_byte_resp(json.dumps(self.response))
        self.wfile.write(resp)
    def send_json_fail(self,code,msg):
        self.response['code'] = code
        self.response['msg'] = msg
        self.response['data'] = {}
        self.set_json_header()
        resp = self.get_byte_resp(json.dumps(self.response))
        self.wfile.write(resp)

class ThreadingHttpServer(ThreadingMixIn,HTTPServer):
    pass
def run():
    port = 3000
    print('starting server, port', port)
    # Server settings
    server_address = ('', port)
    #httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
    httpd = ThreadingHttpServer(server_address,testHTTPServer_RequestHandler)
    print('running server...')
    httpd.serve_forever()

if __name__ == '__main__':
    run()