#!/usr/bin/env python
# -*- coding:utf-8 -*-
#url路径匹配
get_url = {
    #例子
    #"/appname/path":"apis&apiName"
    "/nmap/help":"apis&nmap_help"
}
post_url = {
    #app:nmap
    "/nmap/ping":"apis&nmap_ping",
    "/nmap/ping/range":"apis&nmap_ping_range",
    "/nmap/ping/not/port":"apis&nmap_ping_not_port",
    "/nmap/ack":"apis&nmap_ack",
    "/nmap/traceroute":"apis&nmap_traceroute",
    "/nmap/syn":"apis&nmap_syn",
    "/nmap/tcp":"apis&nmap_tcp",
    "/nmap/udp":"apis&nmap_udp",
    "/nmap/fin":"apis&nmap_fin",
    "/nmap/xmasTree":"apis&nmap_xmasTree",
    "/nmap/null":"apis&nmap_null",
    "/nmap/set/port/range":"apis&nmap_set_port_range",
    "/nmap/specify/port/range":"apis&nmap_specify_port_range",
    "/nmap/os/version":"apis&nmap_os_version",
    "/nmap/all/scan":"apis&nmap_all_scan",
    "/nmap/custom":"apis&nmap_custom",
    #app:dirsearch
    "/dirsearch/scan":"apis&scan_dir",
    "/dirsearch/report":"apis&send_report"

}