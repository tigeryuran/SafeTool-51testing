#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket
import struct
import time
def sendTcpData(ip,port,content):
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((ip,port))
    send_len =len(str.encode(content+"\n"))
    #print(send_len)
    s.send(struct.pack("i",send_len))
    s.send(str.encode(content + "\n"))
    time.sleep(0.5)